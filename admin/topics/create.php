<?php include('../../path.php'); ?>
<?php include('app/controllers/topics.php');?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />

  <!-- Custom Styles -->
  <link rel="stylesheet" href="../../sets/css/styles.css">

  <!-- Admin Styling -->
  <link rel="stylesheet" href="../../sets/css/admin.css">

  <title>Admin - Create Topic</title>
</head>

<body>

  <!-- header -->
  <?php include(ROOT_PATH . "/app/includes/adminHeader.php")?>
  
  <!-- // header -->

  <div class="admin-wrapper clearfix">
    <!-- Left Sidebar -->
    <?php include(ROOT_PATH . "/app/includes/adminSidebar.php")?>
    <!-- // Left Sidebar -->

    <!-- Admin Content -->
    <div class="admin-content clearfix">
      <div class="button-group">
        <a href="create.php" class="btn btn-sm">Add Topic</a>
        <a href="index.php" class="btn btn-sm">Manage Topics</a>
      </div>
      <div class="">
        <h2 style="text-align: center;">Add Topic</h2>

        <form action="create.php" method="post">
        
        <div class="input-group">
            <label>Author</label>
            <input type="text" name="name" type="hidden" value="1">
          </div>
          <div class="input-group">
            <label>Title</label>
            <input type="text" name="title" class="text-input">
          </div>
          <div class="input-group">
            <label>Category</label>
            <input type="text" name="category" class="text-input">
          </div>
          <div class="input-group">
            <label>Image</label>
            <input type="text" name="image" class="text-input">
          </div>
          <div class="input-group">
            <label>Content</label>
            <textarea class="text-input" name="description" id="description"></textarea>
          </div>
          
          <div class="input-group">
            <button type="submit" name="add-topic" class="btn" >Add Topic</button>
          </div>
        </form>

      </div>
    </div>
    <!-- // Admin Content -->

  </div>



  
  <!-- CKEditor 5 -->
  <script src="https://cdn.ckeditor.com/ckeditor5/11.2.0/classic/ckeditor.js"></script>

  <!-- Custome Scripts -->
  <script src="../../assets/js/scripts.js"></script>

</body>

</html>