<header class="clearfix">
<!-- <a href="" class="logo"> -->
    <div class="logo">
      <a href="<?php 'blog/index.php' ?>">
        <h1 class="logo-text"><span>Blog</span>from Scratch</h1>
      </a>
    </div>
    <div class="fa fa-reorder menu-toggle"></div>
    <nav>
      <ul>
        <li><a href="<?php 'index.php' ?>">Home</a></li>
        <!-- <li><a href="#">About</a></li> -->
        <!-- <li><a href="#">Login</a></li> -->
        <!-- <li><a href="register.php">Sign up</a></li> -->
        <li>
          <a href="login.php">
            <i class="fa fa-sign-in"></i>
            Login
          </a>
        </li>
        <li>
          <a href="#" class="userinfo">
            <i class="fa fa-user"></i>
            Authors
            <i class="fa fa-chevron-down"></i>
          </a>
          <ul class="dropdown">
            <li><a href="author.php?id=1">Paul Auchon</a></li>
            <li><a href="author.php?id=2">Manvussa Gérard</a></li>
            <li><a href="author.php?id=3">Harry Cover</a></li>
            
            
          </ul>
        </li>
        <li>
          <a href="#" class="userinfo">
            <i class="fa fa-user"></i>
            Categories
            <i class="fa fa-chevron-down"></i>
          </a>
          <ul class="dropdown">
            <li><a href="category.php?id=API">API</a></li>
            <li><a href="category.php?id=Performance">Performance</a></li>
            <li><a href="category.php?id=IDE">IDE</a></li>
            <li><a href="category.php?id=VS CODE">VS CODE</a></li>
            
          </ul>
        </li>

        <li>
          <!-- <a href="http://localhost:8000/admin/topics/index.php" class="userinfo">
            <i class="fa fa-user"></i>
            Admin
            <i class="fa fa-chevron-down"></i>
          </a> -->
          <ul class="dropdown">
            <li><a href="admin/posts/index.php">Dashboard</a></li>
            <li><a href="#" class="logout">logout</a></li>
          </ul>
         
        </li>
        <div class="search-div">
          <form action="index.php" method="post">
            <input type="text" name="search-term" class="text-input" placeholder="Search...">
          </form>
        </div>
      </ul>
    </nav>
  </header>